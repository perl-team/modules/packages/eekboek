Documentation
-------------
EekBoek documentation (in Dutch only) is installed in
/usr/share/doc/eekboek/html/.  Next to the official EekBoek homepage at
http://www.eekboek.nl/, there's a public Wiki at http://wiki.eekboek.nl/.

Database backends: PostgreSQL vs SQLite
---------------------------------------
If you don't want to use PostgreSQL as back end (but rather SQLite), do not
install eekboek-db-postgresql and be sure your /etc/eekboek/eekboek.conf (or
~/.eekboek/eekboek.conf) has:

 [database]
 driver = sqlite

.

Running ebshell
---------------
The ebshell utility creates and changes files in the current working directory.
So, before running it, cd to e.g. ~/eekboek.

The EB GUI
----------
The EekBoek GUI (ebwxshell) is an extension to the EekBoek Shell (ebshell).  It
is an EekBoek browser, useful for viewing the administrative data.  It's
especially useful for generating, printing and pinpointing reports.
Furthermore, it offers a raw interface to the EekBoek Shell.

The first time ebwxshell is started, it runs in Wizard-mode, helping setting
up a simple book keeping administration in ~/.eekboek.  Various configuration
choices are offered. (NB: this might overwrite older existing eekboek
configuration files!)  Once the Wizard is finished, one is offered the
possibility to continue running the Eekboek GUI in normal mode.  In this mode,
one can generate and view various reports, and one has access to the ebshell.

Once ~/.eekboek is filled, running ebwxshell starts offering a choice between
the various available administrations, as well as the possibility to create a
new administration using the Wizard-mode.  After selecting an administration,
one is offered to generate and view various reports, and one has access to the
ebshell.

One quits ebwxshell by choosing "Bestand", "Afsluiten".

Thanks
------
Thanks to Paul van der Vlis for helping development of this package by testing
it.
